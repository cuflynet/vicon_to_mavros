/****************************************************************************

Conversion from vicon pose/quaternion to mavros pose/quaternion

Nodes:
subscribed pose and quat from Vicon (geometry_msgs::TransformStamped)
published  pose and quat to MAVROS (geometry_msgs::PoseStamped)
both in 

****************************************************************************/

#include <ros/ros.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/PoseStamped.h>
#include <tf/transform_datatypes.h>
#include <tf/LinearMath/Matrix3x3.h>
#include <tf/LinearMath/Quaternion.h>
#include <pthread.h>
#define PI 3.14159265

// p-thread calls
pthread_mutex_t topicBell;
pthread_cond_t newPose;
int vis_count = 0;

// Global Vars
// Visual Pose
geometry_msgs::PoseStamped vis_pos;
// Poll, Pitcy, Yaw
geometry_msgs::Vector3 vicon_rpy;

// Call back Function for Grabbing Vicon Info 
void vicon_trans_Callback(const geometry_msgs::TransformStamped msg)
{
  ++vis_count;
  // Populate Visual Position Time and Header info
  vis_pos.header.seq = vis_count;
  vis_pos.header.stamp = ros::Time::now();
  vis_pos.header.frame_id = "fcu";
  // Populate Visual Position x,y,z translations
  // Convert from Vicon's frame to MAVROS NWU
  vis_pos.pose.position.x = (double)(-1*msg.transform.translation.z);
  vis_pos.pose.position.y = (double)(1*msg.transform.translation.x);
  vis_pos.pose.position.z = (double)(-1*msg.transform.translation.y);
  
  // Grab Orientation Info
  // the incoming geometry_msgs::Quaternion is transformed to a tf::Quaterion
  tf::Quaternion quat(-1*msg.transform.rotation.y,
		              msg.transform.rotation.x,
		              msg.transform.rotation.z,
		              msg.transform.rotation.w);

    
  // the tf::Quaternion has a method to acess roll pitch and yaw
  double roll, pitch, yaw;
  tf::Matrix3x3(quat).getRPY(roll, pitch, yaw);

  // the found angles are written to global RPY for reformatting and eventual repkging
  vicon_rpy.x = roll;
  vicon_rpy.y = pitch;
  vicon_rpy.z = yaw;
  pthread_cond_signal(&newPose);
}

int main(int argc, char **argv)
{ 
  //======= ROS Setup ================
  ros::init(argc, argv, "vic_2_mav_node");
  ros::NodeHandle n;
  //======= Vars ================
  tf::Quaternion vis_quat(0.,0.,0.,1.0);// visual pose quaternion initialization to flat point N  
  //======= ROS Publishers ================
  ros::Publisher pose_publisher = n.advertise<geometry_msgs::PoseStamped>("pose", 1000);
  //======= ROS Subscribers ===============
  // Ros setup async spinners for subscibers
  ros::AsyncSpinner spinner(2);    
  // ROS setup subscriber
  ros::Subscriber trans_subscriber = n.subscribe("transform", 1000, vicon_trans_Callback);
  // Start the Spinner
  spinner.start();
  //ros::spin();
  
  topicBell = PTHREAD_MUTEX_INITIALIZER;
  pthread_cond_init(&newPose, NULL);
  
  while (ros::ok())
  {
    //Only publish when there's a new pose to share
    //Return false if we timed out in the cond_wait
    struct timespec timeout;
    clock_gettime(CLOCK_REALTIME, &timeout);
    timeout.tv_sec +=1;
    int ret;
    ret = pthread_cond_timedwait(&newPose, &topicBell, &timeout);
    if (ret == ETIMEDOUT)
    {
	    ROS_INFO("No Vicon poses received!");
	    continue;
    }
    double mav_roll, mav_pitch, mav_yaw;
    mav_roll = 1*vicon_rpy.x;// mav_roll = -1*vic_yaw;
    mav_pitch = 1*vicon_rpy.y;// mav_pitch = 1*vic_roll;
    mav_yaw = 1*vicon_rpy.z;// mav_yaw = -1*vic_pitch;
	vis_quat.setEulerZYX(mav_yaw,mav_pitch,mav_roll);
	// Populate vis_pose topic:
	vis_pos.pose.orientation.x = (double)vis_quat.x();
	vis_pos.pose.orientation.y = (double)vis_quat.y();
	vis_pos.pose.orientation.z = (double)vis_quat.z();
  	vis_pos.pose.orientation.w = (double)vis_quat.w();    
  	//ROS_INFO("mav rpy angles: roll=%f pitch=%f yaw=%f", mav_roll, mav_pitch, mav_yaw);  	  
  	//ROS_INFO("mav quat: qx=%f qy=%f qz=%f qw=%f", vis_quat.x(), vis_quat.y(), vis_quat.z(), vis_quat.w());
    pose_publisher.publish(vis_pos);
  }
  return 0;
}




